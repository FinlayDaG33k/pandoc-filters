local defs = nil
local tag_matcher = '(.*){{(.+)}}(.*)'
local file_path = "../definitions.txt"

-- Loads our file definitions
-- - Load our specified definition file
-- - Read file line by line
-- - Parse each line and store the definition + meaning in memory
local function load_defs()
  -- Set our defs to an object
  -- This indicates we've already loaded the defs
  defs = {}

  -- Open our definitions file
  -- Return if it cannot be opened
  local f = io.open(file_path, "r")
  if not f then
    return
  end

  -- Loop over each line in the definitions file
  -- Parse each line and store the definition + meaning
  for line in io.lines(file_path) do
    _, _, term, meaning = string.find(line, '-%s+"([a-zA-Z0-9+]+)":%s+"(.+)"')
    defs[string.lower(term)] = meaning
  end
end

-- Main function
-- - Loads our definitions from file
-- - Checks for each word in our Pandoc file that requests a definition
-- - Check for definition
-- - Return definition in a span with a hover title
function def (elem)
  -- Load our definitions if this has not been done yet
  if defs == nil then
    load_defs()
  end

  _, _, leading, content, trailing = string.find(elem.text, tag_matcher)
  if content == nil then
    return elem.text
  end

  -- Check if we have found a definition
  -- If not, return the raw element text
  if defs[string.lower(content)] == nil then
    return elem.text
  end
  
  -- Create our output string
  local output = leading .. [[<span title="]] .. defs[string.lower(content)] .. [[" style="color: red;">]] .. content .. [[</span>]] .. trailing

  -- Return our output string as raw HTML
  return pandoc.RawInline("html", output)
end

return {
  { Str = def }
}
